import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Comentarios from "./pages/Comentarios";

function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/comentarios" exact component={Comentarios} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
