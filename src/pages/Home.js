import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Form from "../components/Form";
import NavBar from "../components/NavBar";

import { getData, initData, latestComments } from "../data/localStorage";

import "./Home.scss";

const CARDS = [
  {
    title: "Descarga tu musica",
    description: "Escucha donde quieras",
    img: "https://i.scdn.co/image/ab671c3d0000f43009302fbaf6259b4c117c704f",
  },
  {
    title: "Música sin anuncios",
    description: "Disfruta sin interrupciones.",
    img: "https://i.scdn.co/image/ab671c3d0000f43098292b95d24a697c20df5137",
  },
  {
    title: "Escucha la canción que quieras",
    description: "Incluso en dispositivos móviles.",
    img: "https://i.scdn.co/image/ab671c3d0000f4306998d3ffd58aad6da6afdf67",
  },
  {
    title: "Saltos ilimitados.",
    description: "Solo pasa a la siguiente.",
    img: "https://i.scdn.co/image/ab671c3d0000f430cd6c528745e510c5be63a012",
  },
];

export default function Home() {
  const [comentarios, setComentarios] = useState(null);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    if (getData() === null) {
      //para iniciar el item DATA en el localStorage la primera vez
      initData();
    } else {
      //setear los comentarios en la variable comentarios y mostrarlos en el html
      async function fechData() {
        await setComentarios(latestComments());
      }
      fechData();
    }
    setReload(false);
  }, [reload]);

  return (
    <div className="home">
      <NavBar />

      <div className="home__fullscreen">
        <div className="home__fullscreen__left">
          <h1>3 meses de Premiun: GRATIS</h1>
          <p>
            Disfruta de la mejor musica y en modo offline, gratis desde hoy.
            Cancela cuando tu quieras
          </p>

          <div className="btns">
            <a href="#">Disfruta 3 meses gratis</a>
            <a href="#">Ver planes</a>
          </div>

          <p className="terms">
            Después, se aplica la tarifa de suscripción mensual. Oferta
            disponible solo para usuarios que todavía no han probado Premium. La
            oferta no incluye los planes Familiar y Duo. Se aplican Términos y
            Condiciones. La oferta termina el 28 de septiembre de 2021.
          </p>
        </div>
        <div className="home__fullscreen__right">
          <img
            src="https://www.popelera.net/wp-content/uploads/2021/08/ed-sheeran-equals.jpg"
            alt=""
          />
        </div>
      </div>

      <div className="home__beneficios">
        <h1>Beneficios de Premium</h1>

        <div className="home__beneficios__grid-container">
          {CARDS.map((item, index) => (
            <div className="grid-item" key={index}>
              <img src={item.img} alt="" />
              <div className="data">
                <h2>{item.title}</h2>
                <p>{item.description}</p>
              </div>
            </div>
          ))}
        </div>
      </div>

      <div className="home__testimonios">
        <div className="home__testimonios__left">
          <Form setReload={setReload} />
        </div>
        <div className="home__testimonios__right">
          {comentarios?.length !== 0 ? (
            <>
              <h1 className="title">Testimonios</h1>
              <Link to="/comentarios">Ver todos los testimonios</Link>
              <div className="grid">
                {comentarios?.map((item, index) => (
                  <div className="grid__item" key={index}>
                    <h1>{item.autor}</h1>
                    <p>{item.comentario}</p>
                    <span>{item.date}</span>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <h2>No hay testinomios</h2>
          )}
        </div>
      </div>
    </div>
  );
}
