import { useEffect, useState } from "react";
import NavBar from "../components/NavBar";
import { allComments } from "../data/localStorage";

import "./Comentarios.scss";

export default function Comentarios() {
  const [comentarios, setComentarios] = useState([]);

  useEffect(() => {
    async function fechData() {
      await setComentarios(allComments());
      console.log(allComments());
    }
    fechData();
  }, []);

  return (
    <div className="comentarios">
      <NavBar />

      <div className="comentarios__content">
        {comentarios.length !== 0 ? (
          <>
            <h1>Todos los comentarios</h1>
            <div className="comentarios__content-grid">
              {comentarios.map((item, index) => (
                <div className="comentarios__content-grid__item" key={index}>
                  <h1>{item.autor}</h1>
                  <p>{item.comentario}</p>
                  <span>{item.date}</span>
                </div>
              ))}
            </div>
          </>
        ) : (
          <h1>No hay Comentarios</h1>
        )}
      </div>
    </div>
  );
}
