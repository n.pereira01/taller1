import "./NavBar.scss";
import { Link } from "react-router-dom";

export default function NavBar() {
  return (
    <div className="nav">
      <div className="nav__left">
        <Link to="/">LOGO</Link>
      </div>
      <div className="nav__right">
        <ul>
          <li>
            <Link to="/comentarios">TESTIMONIOS</Link>
          </li>
          <li>
            <Link to="#">INGRESAR AHORA</Link>
          </li>
        </ul>
      </div>
    </div>
  );
}
