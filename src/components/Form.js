import { useState } from "react";
import { addComment } from "../data/localStorage";
import moment from "moment";

import "./Form.scss";

export default function Form({ setReload }) {
  const [countComent, setCountComent] = useState(0);
  const [formData, setFormData] = useState({
    autor: "",
    comentario: "",
    date: moment().format("YYYY/MM/D hh:mm:ss"),
  });

  const sendData = (e) => {
    e.preventDefault();

    if (countComent > 200) {
      alert(`El comentario es de ${countComent} caracteres (maximo 200)`);
    } else if (!formData.autor || !formData.comentario) {
      alert("El autor o comentario esta vacio");
    } else {
      addComment(formData);
      setReload(true);
      resetForm();
    }
  };

  const resetForm = () => {
    setFormData({
      autor: "",
      comentario: "",
      date: moment().format("YYYY/MM/D hh:mm:ss"),
    });
  };

  return (
    <div className="form__container">
      <h1>Agregar Testimonio</h1>
      <form onSubmit={sendData}>
        <label htmlFor="nombre">Autor</label>
        <input
          type="text"
          name="autor"
          value={formData.autor}
          onChange={(e) => setFormData({ ...formData, autor: e.target.value })}
        />
        <div className="form-area">
          <label htmlFor="comentario">Comentario</label>
          <textarea
            type=""
            name="comentario"
            rows="4"
            cols="50"
            value={formData.comentario}
            onChange={(e) => {
              setFormData({ ...formData, comentario: e.target.value });
              var text = e.target.value;
              setCountComent(text.length);
            }}
          />
          <spam>{countComent}/200</spam>
        </div>

        <button type="submit">Enviar Comentario</button>
      </form>
    </div>
  );
}
