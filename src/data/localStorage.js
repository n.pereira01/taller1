const DATA = "data";

export function addComment(comment) {
  let localArray = JSON.parse(localStorage.getItem(DATA));

  if (localArray.length === 10) {
    localArray.shift();
  }

  localArray.push(comment);
  localStorage.setItem(DATA, JSON.stringify(localArray));
}

export function latestComments() {
  const data = getData();

  var reverse = data.reverse().slice(0, 3);

  return reverse;
}

export function allComments() {
  const data = getData();

  return data.reverse();
}
export function initData() {
  let array = [];
  localStorage.setItem(DATA, JSON.stringify(array));
}

export const getData = () => JSON.parse(localStorage.getItem(DATA));
